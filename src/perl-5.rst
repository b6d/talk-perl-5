:title: Lernen von den Alten - Perl 5
:data-transition-duration: 0
:css: css/solarized.css
:skip-help: true

This is a source file for the `Hovercraft!
<https://pypi.python.org/pypi/hovercraft/>`_ presentation tool.

You can get the latest version from:

https://gitlab.com/b6d/talk-perl-5

To render this presentation to HTML, use the command

.. code:: console

    $ hovercraft perl-5.rst outdir

and then view the outdir/index.html file to see how it turned out.


----


Lernen von den Alten - Gute, interessante und verrückte Ideen in Perl 5
=======================================================================

`Bernhard Weitzhofer
<mailto:bernhardATweitzhofer.org>`_

`fsfe-muc
<https://fsfe-muc.de>`_

`Bayerische Staatsbibliothek
<https://www.bsb-muenchen.de>`_

`gitlab.com/b6d/talk-perl-5
<https://gitlab.lrz.de/b6d/talk-perl-5>`_

----

* Erfahrung in Perl?

* Write Only Language?

* 3[12]C3: The Perl Jam [2]?

----

It's 2017. Why do you still code Perl 5?
========================================

----

Fun
===

* TIMTOWTDI ("Tim Toady")
* DWIM
* Kontext

----

Präambel
========

.. code:: perl

    #!/usr/bin/env perl
    use strict
    use warnings
    use 5.024
    use English

----

Crashkurs Datentypen
====================

.. code:: perl

    # Skalar
    my $foo = "Hallo";

    # Array
    my @words = ("Hallo", "Welt");

    # Hash
    my %numbers_1 = (Alice => 123,
                     Bob   => 456);

    # Elementzugriff
    my $foo = $words[0];
    my $bar = $numbers{Alice};

----

Crashkurs Subroutinen (Funktionen)
==================================

.. code:: perl

    sub greet {
        say "Hello Wold";
    }

----

TimToady: Quoting
=================

.. code:: perl

    my $foo = q(Herta's "Schnell"-Imbiss)
    my @bar = qw(foo bar baz)
    my @baz = qw|foo bar baz|

----

Kontext: Default-Variablen
==========================

.. code:: perl

    my @words = qw(foo bar baz)

    foreach my $foo (@words) {
        say $foo;
    }

    foreach (@words) {
        say $ARG;
    }

    foreach (@words) {
        say $_;
    }

----

TimToady: Statement Modifiers
=============================

.. code:: perl

    foreach (@words) {
        say $_;
    }

    say $_ foreach @words;

----

Kontext: Default-Variablen 2
============================

.. code:: perl

    # Subroutinenargumente in @ARG
    sub greet2 {
        my  $name = $ARG[0];
        say "Hello, $name!";
    }

----

TimToady: Subroutinen-Argumente
===============================

.. code:: perl

    # positional
    sub greet3 {
        my  ($greeting, $name) = @ARG;
        say "$greeting, $name!";
    }
    greet3("Hello", "World");

    # keyword
    sub greet4 {
        my   %kw = @ARG;
        say "$kw{greeting}, $kw{name}!";
    }
    greet4(greeting=>"Hello", name=>"World");

----

Kontext: String vs. Numeric
===========================

.. code:: perl

    my $foo = "0.0"
    my $bar = "0"
    $foo == $bar
    $foo eq $bar

----

Kontext: Skalar Vs. List
========================

.. code:: perl

    my @foo = qw(foo bar baz);

    @foo;        # ('foo', 'bar', 'baz')
    scalar @foo; # 3

    for(my $i=0; $i<@foo; $i++) {
        ...
    }

----

Kontext: Skalar Vs. List
========================

.. code:: perl

    sub giveme {
        return qw(foo bar baz) if wantarray;
        return "qup";
    }
    my $a = giveme();
    # "qup"

    my @a = giveme();
    # ("foo", "bar", "baz")

----

Kontext: Skalar Vs. List
========================

.. code:: perl

    my $does_match = "foo bar" =~ /(\w+)/g
    # 1

    my @matches    = "foo bar" =~ /(\w+)/g
    # ("foo", "bar")

----

DWIM: Prototypen
================

.. code:: perl

    sub inv ($)  { return 1       / $ARG[0] }
    sub add ($$) { return $ARG[0] + $ARG[1] }

    my @foo = (inv 3, 4, add 5, 6);

----

TimToady: Subroutinen: Rekursion
================================

.. code:: perl

    sub add {
        my($m, $n) = @ARG;
        return $n if $m == 0;
        return add(--$m, ++$n)
    }

----

TimToady: Subroutinen: TCO
==========================

.. code:: perl

    sub add {
        my($m, $n) = @ARG;
        return $n if $m == 0;
        @ARG = (--$m, ++$n)
        goto &add;
    }

----

WTH: subs as lvalues
====================

.. code:: perl

    my $VAL = 42;

    sub val : lvalue {
        say 'Zugriff auf $VAL';
        return $VAL;
    }

    say val();

    say val() = 44;

----

WTH: 1-based indexing
=====================

.. code:: perl

    use 5.008;

    my @foo = qw(foo bar baz)
    say $foo[0];

    $[ = 1;
    say $foo[1];

