Folien zu: "Lernen von den Alten - [...] Ideen in Perl 5"
=========================================================

`Open-Source-Treffen 27.10.2017 <https://www.opensourcetreffen.de>`_

`gitlab.com/b6d/talk-perl-5 <https://gitlab.com/b6d/talk-perl-5>`_


Folien anzeigen
---------------

Die Folien können mit dem Browser angezeigt werden:

.. code:: console

    $ cd talk-perl-5/
    $ firefox html/index.html


Folien neu erzeugen
-------------------

Hierfür wird `Python <https://python.org>`_ 3 und der Python-Paketmanager pip_
(bei Python 3 üblicherweise dabei) benötigt:

.. code:: console

    $ pip3 install hovercraft
    $ hovercraft src/perl-5.rst html/


.. Links:

.. _Git: https://git-scm.org
.. _Gitlab: https://gitlab.com
.. _pip: https://pip.pypa.io
.. _Python: https://www.python.org
